# Ailisonsextractor

Welcome to my GPS coordinates extractor. This app will help you extract coordinates from image files with EXIF metadata 
place it on a CSV file.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ailisonsextractor'
```

And then execute:

    $ bundle

## Usage

    Usage: extract [options]
        -p, --path PATH   Path with images for EXIF extraction
        -f                Export file in HTML format instead of CSV


## Development

Run `bin/setup` to install dependencies. Then, run `rake test` to run the tests.

To install this gem onto your local machine, run `bundle exec rake install`.

## Assumptions

* Gem is not going to be used for large amount of images, over 100,000.
* It will use an export folder inside the gem for the exported data. (In the future it will ask for an Amazon S3 credentials)
* If any application comes to use this gem it uses exit codes following recommendations from FreeBSD https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3.
* Log over console.
* App ignores images without coordinates.
* App isn't going to be extended to extract other info. So, I didn't use a command suite like GLI to make it more robust.

## Improvements
* I will apply so good OO into this GEM. It is a mess of one sitting I did a while ago.
