lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "ailisonsextractor/version"

Gem::Specification.new do |spec|
  spec.name          = "ailisonsextractor"
  spec.version       = Ailisonsextractor::VERSION
  spec.authors       = ["ailison"]
  spec.email         = ["ailison@hotmail.com"]

  spec.summary       = %q{This APP extracts lat/lon from images with EXIF data to a CSV/HTML file.}
  spec.description   = %q{This APP uses the gem EXIFR to extract lat/long data from images that have available EXIF data
(JPG and TIF) and export them to a CSV or HTML file.}

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "exifr", "~>1.3.0"
  spec.add_development_dependency "ruby-progressbar", "~>1.10.1"
end
