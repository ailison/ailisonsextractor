require 'ailisonsextractor/version'
require 'ailisonsextractor/coordinate'

module Ailisonsextractor
  class Error < StandardError; end
  class NoLatLon < StandardError; end
end
