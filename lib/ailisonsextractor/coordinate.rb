module Ailisonsextractor
  class Coordinate
    # Class params
    attr_reader :latitude,
                :longitude,
                :name

    # Coordinate construction
    # == Params:
    # * +ip+: Image path
    def initialize(ip)
      begin
        # Check if current image is JPG/JPEG or TIF/TIFF
        case ip.split('.').last.downcase
        when /(jpg|jpeg)$/i;
          a = EXIFR::JPEG.new(ip).gps
        when /(tif|tiff)$/i;
          a = EXIFR::TIFF.new(ip).gps
        else
          raise ArgumentError, "It only accepts jpg and tif images."
        end

        # If there are GPS coordinate
        unless a.nil?
          @latitude = a.latitude
          @longitude = a.longitude
          @name = ip.to_s.split('/').last

          # GPS coordinate not found raises custom exception
        else
          raise NoLatLon, "Image doesn't have lat/lon."
        end
      end
    end

    # Validates and raises exceptions if args not valid
    # == Params:
    # * +options+: OptionParser options
    # * +collection+: Coordinate collection
    def self.validate_args(options,collection)
      unless options.is_a?(Hash) and options[:ip] and options[:ip].is_a? Pathname
        raise ArgumentError, "The options are invalid."
      end
      if collection.empty?
        raise ArgumentError, "The collection is empty."
      end
      collection.each do |line|
        unless line.instance_of? Ailisonsextractor::Coordinate
          raise ArgumentError, "The collection of coordinates aren't valid."
        end
      end
    end

    # Exports a collection of Coordinates to a CSV file
    # == Params:
    # * +options+: OptionParser options
    # * +collection+: Coordinate collection
    def self.export_csv(options,collection)
      validate_args(options,collection)

      # Create export file and adds data / JOIN project dir + export path + file name (image path given downcased split with - and date)
      file = File.join(File.dirname(__FILE__).gsub("lib/ailisonsextractor",""),
                       "./export/#{options[:ip].realpath.to_s.gsub("/","-").downcase[1..-1]}-#{DateTime.now.strftime('%Y%m%dT%H%M%S')}.csv")

      # Write Coordinate by Coordinate lat/lon into file
      CSV.open(file, 'wb') do |f|
        collection.each do |line|
          f << [line.name, line.latitude, line.longitude]
        end
      end

      # Return file name with path inside project
      return file.split('/ailisonsextractor/').last
    end

    # Exports a collection of Coordinates to an HTML file
    # == Params:
    # * +options+: OptionParser options
    # * +collection+: Coordinate collection
    def self.export_html(options,collection)
      validate_args(options,collection)

      # Layout vars
      @path = options[:ip].realpath
      @time = DateTime.now
      @data = collection

      # Load template
      template = File.read('../lib/ailisonsextractor/templates/export.html.erb')
      result = ERB.new(template).result(binding)

      # Create export file and adds data / JOIN file dir + file name (image path given downcased split with - and date)
      file = File.join(File.dirname(__FILE__).gsub("lib/ailisonsextractor",""),
                       "./export/#{options[:ip].realpath.to_s.gsub("/","-").downcase[1..-1]}-#{DateTime.now.strftime('%Y%m%dT%H%M%S')}.html")

      # Write built and processed layout on file
      File.open(file, 'wb') do |f|
        f.write result
      end

      # Return file name with path inside project
      return file.split('/ailisonsextractor/').last
    end
  end
end
