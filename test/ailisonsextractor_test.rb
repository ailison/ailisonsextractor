require 'test_helper'
require 'ailisonsextractor'
require 'exifr/jpeg'
require 'exifr/tiff'
require 'csv'

class AilisonsextractorTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Ailisonsextractor::VERSION
  end

  def test_raise_with_wrong_images_path
    assert_raises SystemCallError do
      Ailisonsextractor::Coordinate.new('./doesntexist.jpg')
    end
  end

  def test_raise_with_wrong_image_format
    e = assert_raises ArgumentError do
      Ailisonsextractor::Coordinate.new('./invalidformat.png')
    end
    assert_equal "It only accepts jpg and tif images.", e.message
  end

  def test_raise_image_without_EXIF_data
    e = assert_raises Ailisonsextractor::NoLatLon do
      Ailisonsextractor::Coordinate.new('./test/image/noexif.jpg')
    end
    assert_equal "Image doesn't have lat/lon.", e.message
  end

  def test_raise_passing_invalid_options_var
    e = assert_raises ArgumentError do
      Ailisonsextractor::Coordinate.export_csv('nothing',{})
    end
    assert_equal "The options are invalid.", e.message
  end

  def test_raise_passing_options_hash_without_ip
    e = assert_raises ArgumentError do
      Ailisonsextractor::Coordinate.export_csv({noip: ''},{})
    end
    assert_equal "The options are invalid.", e.message
  end

  def test_raise_passing_empty_collection
    e = assert_raises ArgumentError do
      Ailisonsextractor::Coordinate.export_csv({ip: Pathname.new('./gps_images')},{})
    end
    assert_equal "The collection is empty.", e.message
  end

  def test_raise_passing_collection_with_invalid_hashes
    coordinate = Ailisonsextractor::Coordinate.new('./test/image/valid.jpg')
    e = assert_raises ArgumentError do
      Ailisonsextractor::Coordinate.export_csv({ip: Pathname.new('./gps_images')},[coordinate,nil])
    end
    assert_equal "The collection of coordinates aren't valid.", e.message
  end

  def test_exported_file_path_return
    coordinate = Ailisonsextractor::Coordinate.new('./test/image/valid.jpg')
    path = Ailisonsextractor::Coordinate.export_csv({ip: Pathname.new('./gps_images')},[coordinate])
    assert File.file?(path)
  end

  def test_lat_lon_is_correct_from_test_file
    coordinate = Ailisonsextractor::Coordinate.new('./test/image/valid.jpg')
    assert_equal 38.4,coordinate.latitude
    assert_equal -122.82866666666666,coordinate.longitude
  end

  # @TODO validate export_html method
end
